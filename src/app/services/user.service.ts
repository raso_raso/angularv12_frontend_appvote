import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private userUrl = 'http://localhost:8080/api/test/user';
  private candidatesByIdUrl = 'http://localhost:8080/api/test/testing';

  constructor(private http: HttpClient) { }

  getUserBoard(): Observable<any> {
    return this.http.get(this.userUrl);
  }

  getCandidatesById(): Observable<any> {
    return this.http.get(this.candidatesByIdUrl);
  }

}
