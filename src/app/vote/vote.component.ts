import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})

export class VoteComponent implements OnInit {
  candidate: any;

  constructor(private  http: HttpClient, private userService: UserService) { }

  ngOnInit() {
    this.userService.getCandidatesById().subscribe(
      data => {
        this.candidate = {
          data
        };
      },
    );
  }


}
