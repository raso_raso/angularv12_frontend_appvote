import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../auth/token-storage.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  info: any;
  userInfo: any;
  candidate: any;
  errorMessage: string | undefined;

  constructor(private token: TokenStorageService, private userService: UserService) {  }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      email: this.token.getEmail(),
    };
    this.userService.getUserBoard().subscribe(
      data => {
        this.userInfo = {
          email: data.user.email,
          name: data.user.name,
        };
      },
      error => {
        this.errorMessage = `${error.status}: ${error.error}`;
      }
    );
    // get candidates by id
    this.userService.getCandidatesById().subscribe(
      data => {
        this.candidate = {
          data
        };
      },
    );
  }
}
