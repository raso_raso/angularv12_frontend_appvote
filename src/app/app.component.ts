import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './auth/token-storage.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  authority: string | undefined;
  info: any;
  userInfo: any;
  errorMessage: string | undefined;

  constructor(private tokenStorage: TokenStorageService, private token: TokenStorageService, private userService: UserService) { }

  // @ts-ignore
  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.authority = 'user';
      this.info = {
        token: this.token.getToken(),
        email: this.token.getEmail(),
      };
      this.userService.getUserBoard().subscribe(
        data => {
          this.userInfo = {
            email: data.user.email,
            name: data.user.name
          };
        },
        error => {
          this.errorMessage = `${error.status}: ${error.error}`;
        }
      );
      return true;
    }
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }
}
