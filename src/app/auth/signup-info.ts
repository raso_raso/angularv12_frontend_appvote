export class SignUpInfo {
    name: string;
    email: string;
    password: string;
    identificationNumber: string;
    typeDocument: string;

    constructor(name: string, email: string, password: string, identificationNumber: string, typeDocument: string) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.identificationNumber = identificationNumber;
        this.typeDocument = typeDocument;
    }
}
