export class JwtResponse {
    accessToken: string | undefined;
    email: string | undefined;
}
